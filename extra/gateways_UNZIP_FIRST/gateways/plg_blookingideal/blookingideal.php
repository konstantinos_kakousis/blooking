<?php
/**
 * BlooKing - The King in Joomla Booking
 * @version 1.0
 * @author BlooKing.org. Based on BooKiTGold code by Costas Kakousis
 * @copyright (C) 2012-2013 by BlooKing.org (http://www.blooking.org)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 * If you fork this to create your own project, please make a reference to 
 * BlooKing! someplace in your code and provide a link to 
 * http://www.blooking.org
 *
 * This file is part of BlooKing! BlooKing is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * BlooKing is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. You should have received a copy of the GNU General Public License
 * along with BlooKing.  If not, see <http://www.gnu.org/licenses/>.
 *   ______ __                __  __ __               
 *	|   __ \  |.-----..-----.|  |/  |__|.-----..-----.
 *	|   __ <  ||  _  ||  _  ||     <|  ||     ||  _  |
 *	|______/__||_____||_____||__|\__|__||__|__||___  |
 *                                             |_____|
 **/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemBlookingIdeal extends JPlugin
{
	private $iDEAL=null;
	function plgSystemBlookingIdeal (&$subject, $config)
	{
		parent::__construct($subject, $config);
		
	}
	
	function onMakeBookingBlookingideal($data)
	{
		$res = new stdClass ();
		$this->loadLanguage('plg_system_blookingideal', JPATH_ADMINISTRATOR);

		if ($data['deposit']<=0)
		{
			$res->msg = "<span class='error'>".JText::_('COM_BOOKIT_WRONG_AMOUNT')."</span>";
			$res->code = -1;
			return $res;
		} 
		//Get merchant details from plugin params
		$partner_id = $this->params->get('partner_id', '00000');
		
		require_once('ideal.class.php');
		$this->iDEAL = new iDEAL_Payment ($partner_id);
		
		
		$res->msg="";
		//if (!in_array('ssl', stream_get_transports()))
		//{
		//	$res->msg.= "<h1>Foutmelding</h1>";
		//	$res->msg.= "<p>Uw PHP installatie heeft geen SSL ondersteuning. SSL is nodig voor de communicatie met de Mollie iDEAL API.</p>";
		//	$res->code = -1;
		//	return $res;
		//}
		//$this->iDEAL->setTestMode();
		
		$bank_array = $this->iDEAL->getBanks();
		
		if ($bank_array == false)
		{
			$res->msg.=  "<p>Er is een fout opgetreden bij het ophalen van de banklijst: ". $this->iDEAL->getErrorMessage(). "</p>";
			$res->code = -1;
			return $res;
		}
		
		
		$res->msg.=  "<form method='post' id='idealForm'>
			<select name='bank_id' id='bank_id'>
				<option value=''>Kies uw bank</option>";
		
		 foreach ($bank_array as $bank_id => $bank_name) { 
			$res->msg.=  "<option value='".$bank_id."'>".$bank_name."</option>";
		 } 
		
		$res->msg.= "</select>";
		$res->msg .= "<input type='hidden' name='amount' value='".$data['deposit']."'>";
		$res->msg.= "<input type='hidden' name='custom' id='bookingids' value=''>";
		
		$res->msg.= "</form>";
		$res->code = 1;
		
		return $res;
	}

	function onClickBookingBlookingideal($data)
	{
		$res = new stdClass ();
		$res->msg ="";
		if (!isset($this->iDEAL))
		{
			//Get merchant details from plugin params
			$partner_id = $this->params->get('partner_id', '000000');
			require_once('ideal.class.php');
			$this->iDEAL = new iDEAL_Payment ($partner_id);
		}

		$description = $this->params->get('item_name', 'Testbetaling');
		$report_url = JURI::base()."index.php?option=com_blooking&lang=".$data['lang']."&controller=finalform&task=idealtransactionCompleted";
		
		//CONVERT EUR TO CENT
		$amount = $data['amount']*100;
		
		if (isset($data['bank_id']) and !empty($data['bank_id']))
		{
			if ($this->iDEAL->createPayment($data['bank_id'], $amount, 
					$description, $report_url, $report_url)) {
				
// 				$app =& JFactory::getApplication();
// 				$app->redirect($this->iDEAL->getBankURL());
// 				return true;
				
				$res->msg="OK";
				$res->url = $this->iDEAL->getBankURL();
				return $res;
				
			}
			else
			{
				echo $this->iDEAL->getErrorMessage();
				
				$res->msg.= '<p>De betaling kon niet aangemaakt worden.</p>';
				$res->msg.=  '<p><strong>Foutmelding:</strong> '.$this->iDEAL->getErrorMessage().'</p>';
				
				//RETURN $RES DOES NOT DISPLAY
				return $res;
			}
		}
		
		return $res;
		
	}
	
}