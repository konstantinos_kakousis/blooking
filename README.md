**Blooking has been discontinued. If you are interested in Joomla 3.x Property booking have a look at [JoomlaToys.com](http://www.joomlatoys.com)**

# BlooKing

### Installing

	1.  You can install the component and the side module directly from the folder com_blooking_vx.x.x.zip
	2.  You can find translations in the translations_UNZIP_FIRST.zip. Unzip the archive and find the language you wish to apply. Languages kept in zip files can be install directly while others in files need to be uploaded in your languages folder: site/languages 
	3.  The extra folder includes the mod_blookingavailability.zip that shows only the availability calendar for a preselected category. The gateways_UNZIP_FIRST.zip folder contains plugins that implement the interconnection with other payment systems besides Paypal. Includes support for asiapay, cimb, egipsy, mollie-ideal, robokassa and sermepa. The blookingcash plugin can be used in situations you wish to allow full payment upon arrival.
	
## Demo ##

[BlooKing Demo](http://angrypeng.com/blooking)